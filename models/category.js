const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
    name: {
        type: String,
        require : true,
    }, 
    icon : {
        type : String,
    },
    color : {
        type : String,
    }
})

//exports digunakan agara model dapat dipakai di semua file
exports.Category = mongoose.model("Category", categorySchema);
