const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const morgan = require("morgan");
const mongoose = require("mongoose");
const cors = require('cors');
require("dotenv/config");
const authJwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');

//enable cors
app.use(cors());
app.options('*',cors());
//middleware
app.use(bodyParser.json()); 
app.use(morgan("tiny"));
app.use(authJwt());
app.use('/public/uploads', express.static(__dirname + '/public/uploads'));
app.use(errorHandler);



//routes
const categoriesRouters = require("./routers/categories");
const productsRouters = require("./routers/products");
const usersRouters = require("./routers/users");
const ordersRouters = require("./routers/orders");

const api = process.env.API_URL;

app.use(`${api}/categories`, categoriesRouters);
app.use(`${api}/products`, productsRouters);
app.use(`${api}/users`, usersRouters);
app.use(`${api}/orders`, ordersRouters);

 
mongoose.connect(process.env.CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("database connection is ready....");
  })
  .catch((err) => {
    console.log(err);
  });

app.listen(3000, () => {
  console.log("servers is running http://localhost:3000");
});
